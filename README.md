# @uiii-lib prettier config

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install

```
yarn add --dev prettier @uiii-lib/prettier-config
```

And put into `prettier.config.js`

```js
import prettierConfig from "@uiii-lib/prettier-config";

export default prettierConfig;
```

> For more option how to use visit https://prettier.io/docs/en/configuration.html#sharing-configurations

It is recommended to also install [@uiii-lib/eslint-config](https://gitlab.com/uiii-lib/eslint-config)
